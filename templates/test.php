<?php

use ProPhp\TestsLite\Helper;

$location = Helper::getLocation();

$actualOutput = Helper::getInputData($location); // process input data

$expectedOutput = Helper::getOutputData($location);

Helper::assertEquals($actualOutput, $expectedOutput);