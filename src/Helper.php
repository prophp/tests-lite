<?php

namespace ProPhp\TestsLite;

class Helper
{
    public static function getDataContents(string $location, string $ioType, string $ext = 'txt')
    {
        return file_get_contents(Config::webrootPath() . Config::testsDirTitle() . "/data/$location/$ioType.$ext");
    }

    public static function getDataDirPath(string $location)
    {
        return Config::webrootPath() . Config::testsDirTitle() . "/data/$location";
    }

    public static function getInputData(string $location, string $ext = 'txt')
    {
        return self::getDataContents($location, 'input', $ext);
    }

    public static function getOutputData(string $location, string $ext = 'txt')
    {
        return self::getDataContents($location, 'output', $ext);
    }

    public static function getLocation()
    {
        $testFileFullPath = debug_backtrace()[1]['args'][0];

        return substr($testFileFullPath, strlen(Config::webrootPath() . Config::testsDirTitle() . "/run/"), -4);
    }

    public static function assertEquals(mixed $actualOutput, mixed $expectedOutput, string $displayContents = null){
        if ($actualOutput !== $expectedOutput) {

            echo "$displayContents\n";

            echo "ACTUAL" . PHP_EOL;
            var_dump($actualOutput); // @todo Use prophp/debugger's debug()
            echo "EXPECTED" . PHP_EOL;
            var_dump($expectedOutput); // @todo Use prophp/debugger's debug()
            throw new \Exception("Actual output is not same with an expected one");
        }
    }
}

