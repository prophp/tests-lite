<?php

namespace ProPhp\TestsLite;

class Config
{
    private static function getConfig()
    {
        $configFilePath = dirname(__DIR__, 4) . "/config/tests-lite.json";

        return file_exists($configFilePath) ?
            json_decode(file_get_contents($configFilePath), true) :
            [];
    }

    public static function webrootPath()
    {
        return self::getConfig()['webrootPath'] ?? "/var/www/html/";
    }

    public static function testsDirTitle()
    {
        return self::getConfig()['testsDirTitle'] ?? "tests";
    }
}